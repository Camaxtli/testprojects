package com.example.SensorsGraphics;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;

import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.tools.PanListener;
import org.achartengine.tools.ZoomEvent;
import org.achartengine.tools.ZoomListener;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends Activity implements SensorEventListener {

    private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();

    private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();

    private XYSeries mXSeries;
    private XYSeries mYSeries;
    private XYSeries mZSeries;
    private XYSeries mForceSeries;

    private XYSeriesRenderer mCurrentRenderer;

    private String mDateFormat;

    public class Point {
        public float x;
        public float y;
        public float z;
        public float force;
    }

    volatile List<Point> pointsList = new ArrayList<Point>();
    ReentrantLock pointsListLock = new ReentrantLock();

    private GraphicalView mChartView;

    SensorManager mSensorManager;
    Sensor mAccelSensor;
    Sensor mLinAccelSensor;
    Sensor mGyroscopeSensor;

    int samplesNumber = 1000;
    int count;

    float x0, y0, z0;
    float x0vals[], y0vals[], z0vals[];

    private List<Point> getOldPointsList() {
        pointsListLock.lock();
        List<Point> oldPointsList = pointsList;
        pointsList = new ArrayList<Point>();
        pointsListLock.unlock();

        return oldPointsList;
    }

    private void addPoint(Point point) {
        pointsListLock.lock();
        pointsList.add(point);
        pointsListLock.unlock();
    }

    private int index = 0;

    static double x = 0;
    static double y = 0;

    protected Update mUpdateTask;

    @Override
    protected void onRestoreInstanceState(Bundle savedState) {
        super.onRestoreInstanceState(savedState);
        mDataset = (XYMultipleSeriesDataset) savedState
                .getSerializable("dataset");
        mRenderer = (XYMultipleSeriesRenderer) savedState
                .getSerializable("renderer");
        //mCurrentSeries = (XYSeries) savedState
        //		.getSerializable("current_series");
        mXSeries = (XYSeries) savedState
                .getSerializable("xx_series");
        mYSeries = (XYSeries) savedState
                .getSerializable("xy_series");
        mZSeries = (XYSeries) savedState
                .getSerializable("xz_series");
        mCurrentRenderer = (XYSeriesRenderer) savedState
                .getSerializable("current_renderer");
        mDateFormat = savedState.getString("date_format");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("dataset", mDataset);
        outState.putSerializable("renderer", mRenderer);
        //outState.putSerializable("current_series", mCurrentSeries);
        outState.putSerializable("xx_series", mXSeries);
        outState.putSerializable("xy_series", mYSeries);
        outState.putSerializable("xz_series", mZSeries);
        outState.putSerializable("current_renderer", mCurrentRenderer);
        outState.putString("date_format", mDateFormat);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mRenderer.setApplyBackgroundColor(true);
        mRenderer.setBackgroundColor(Color.argb(100, 50, 50, 50));
        mRenderer.setAxisTitleTextSize(16);
        mRenderer.setChartTitleTextSize(20);
        mRenderer.setLabelsTextSize(15);
        mRenderer.setLegendTextSize(15);
        mRenderer.setMargins(new int[]{20, 30, 15, 0});
        mRenderer.setZoomButtonsVisible(true);
        mRenderer.setPointSize(10);
        mRenderer.setXTitle("TIME");
        mRenderer.setYTitle("y");
        mRenderer.setShowGrid(true);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mLinAccelSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mGyroscopeSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        String seriesTitle1 = "Series X";
        String seriesTitle2 = "Series Y";
        String seriesTitle3 = "Series Z";
        String seriesTitle4 = "Force";
        XYSeries xSeries = new XYSeries(seriesTitle1);
        XYSeries ySeries = new XYSeries(seriesTitle2);
        XYSeries zSeries = new XYSeries(seriesTitle3);
        XYSeries forceSeries = new XYSeries(seriesTitle4);
        mDataset.addSeries(xSeries);
        mDataset.addSeries(ySeries);
        mDataset.addSeries(zSeries);
        mDataset.addSeries(forceSeries);
        mXSeries = xSeries;
        mYSeries = ySeries;
        mZSeries = zSeries;
        mForceSeries = forceSeries;

        XYSeriesRenderer renderer1 = new XYSeriesRenderer();
        renderer1.setPointStyle(PointStyle.POINT);
        renderer1.setColor(Color.BLUE);
        renderer1.setFillPoints(true);
        XYSeriesRenderer renderer2 = new XYSeriesRenderer();
        renderer2.setPointStyle(PointStyle.POINT);
        renderer2.setColor(Color.RED);
        renderer2.setFillPoints(true);
        XYSeriesRenderer renderer3 = new XYSeriesRenderer();
        renderer3.setPointStyle(PointStyle.POINT);
        renderer3.setColor(Color.GREEN);
        renderer3.setFillPoints(true);
        XYSeriesRenderer renderer4 = new XYSeriesRenderer();
        renderer4.setPointStyle(PointStyle.POINT);
        renderer4.setColor(Color.WHITE);
        renderer4.setFillPoints(true);

        mRenderer.addSeriesRenderer(renderer1);
        mRenderer.addSeriesRenderer(renderer2);
        mRenderer.addSeriesRenderer(renderer3);
        mRenderer.addSeriesRenderer(renderer4);

        mUpdateTask = new Update();
        mUpdateTask.execute(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //mSensorManager.registerListener(this, mAccelSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mLinAccelSensor, SensorManager.SENSOR_DELAY_GAME);
        //mSensorManager.registerListener(this, mGyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL);

        count = 0;

        x0 = 0;
        y0 = 0;
        z0 = 0;

        x0vals = new float[samplesNumber];
        y0vals = new float[samplesNumber];
        z0vals = new float[samplesNumber];

        if (mChartView == null) {
            LinearLayout layout = (LinearLayout) findViewById(R.id.chart);
            mChartView = ChartFactory.getLineChartView(this, mDataset,
                    mRenderer);
            mRenderer.setClickEnabled(true);
            mRenderer.setSelectableBuffer(100);

            mChartView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SeriesSelection seriesSelection = mChartView
                            .getCurrentSeriesAndPoint();
                    double[] xy = mChartView.toRealPoint(0);
                    if (seriesSelection == null) {
//                        Toast.makeText(XYChartBuilder.this,
//                                "No chart element was clicked",
//                                Toast.LENGTH_SHORT).show();
                    } else {
//                        Toast.makeText(
//                                XYChartBuilder.this,
//                                "Chart element in series index "
//                                        + seriesSelection.getSeriesIndex()
//                                        + " data point index "
//                                        + seriesSelection.getPointIndex()
//                                        + " was clicked"
//                                        + " closest point value X="
//                                        + seriesSelection.getXValue() + ", Y="
//                                        + seriesSelection.getValue()
//                                        + " clicked point value X="
//                                        + (float) xy[0] + ", Y="
//                                        + (float) xy[1], Toast.LENGTH_SHORT)
//                                .show();
                    }
                }
            });
            mChartView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    SeriesSelection seriesSelection = mChartView
                            .getCurrentSeriesAndPoint();
                    if (seriesSelection == null) {
//                        Toast.makeText(XYChartBuilder.this,
//                                "No chart element was long pressed",
//                                Toast.LENGTH_SHORT);
//                        return false; // no chart element was long pressed, so
                        // let something
                        // else handle the event
                    } else {
//                        Toast.makeText(XYChartBuilder.this,
//                                "Chart element in series index "
//                                        + seriesSelection.getSeriesIndex()
//                                        + " data point index "
//                                        + seriesSelection.getPointIndex()
//                                        + " was long pressed",
//                                Toast.LENGTH_SHORT);
//                        return true; // the element was long pressed - the event
                        // has been
                        // handled
                    }
                    return true;
                }
            });

            mChartView.addZoomListener(new ZoomListener() {
                public void zoomApplied(ZoomEvent e) {
                    String type = "out";
                    if (e.isZoomIn()) {
                        type = "in";
                    }
                    System.out.println("Zoom " + type + " rate "
                            + e.getZoomRate());
                }

                public void zoomReset() {
                    System.out.println("Reset");
                }
            }, true, true);

            mChartView.addPanListener(new PanListener() {
                public void panApplied() {
                    System.out.println("New X range=["
                            + mRenderer.getXAxisMin() + ", "
                            + mRenderer.getXAxisMax() + "], Y range=["
                            + mRenderer.getYAxisMax() + ", "
                            + mRenderer.getYAxisMax() + "]");
                }
            });
            layout.addView(mChartView, new LayoutParams(
                    LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
        } else {
            mChartView.repaint();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] values = event.values;
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER: {
                //mXValueText.setText(String.format("%1.3f", values[0]));
                //mYValueText.setText(String.format("%1.3f", values[1]));
                //mZValueText.setText(String.format("%1.3f", values[2]));

                Point point = new Point();
                point.x = values[0];
                point.y = values[1];
                point.z = values[2];

                Log.d("Debug_data", "ACCEL x=" + values[0] + "y=" + values[1] + "z=" + values[2]);
                addPoint(point);

                double totalForce = 0.0f;
                totalForce += Math.pow(values[0] / SensorManager.GRAVITY_EARTH, 2.0);
                totalForce += Math.pow(values[1] / SensorManager.GRAVITY_EARTH, 2.0);
                totalForce += Math.pow(values[2] / SensorManager.GRAVITY_EARTH, 2.0);

                totalForce = Math.sqrt(totalForce);
                //mForceValueText.setText(String.format("%1.3f", totalForce));
            }
            break;
            case Sensor.TYPE_LINEAR_ACCELERATION: {
                if (count <= samplesNumber) {
                    if (count < samplesNumber) {
                        x0vals[count] = values[0];
                        y0vals[count] = values[1];
                        z0vals[count] = values[2];
                        count++;
                        break;
                    } else {
                        for (int i = 0; i < samplesNumber; i++) {
                            x0 += x0vals[i];
                            y0 += y0vals[i];
                            z0 += z0vals[i];
                        }
                        x0 /= samplesNumber;
                        y0 /= samplesNumber;
                        z0 /= samplesNumber;

                        count++;
                    }
                }

                Point point = new Point();
                point.x = values[0] - x0;
                point.y = values[1] - y0;
                point.z = values[2] - z0;

                point.force = (float)Math.sqrt(Math.pow(point.x, 2) + Math.pow(point.y, 2) + Math.pow(point.z, 2));

                addPoint(point);

                Log.d("Debug_data", "LIN ACCEL x=" + point.x + "y=" + point.y + "z=" + point.z);

            }
            break;
            case Sensor.TYPE_GYROSCOPE: {
                Point point = new Point();
                point.x = values[0];
                point.y = values[1];
                point.z = values[2];

                Log.d("Debug_data", "GYROSCOPE x=" + point.x + "y=" + point.y + "z=" + point.z);

                addPoint(point);
            }
            break;
        }
    }

    protected class Update extends AsyncTask<Context, Integer, String> {

        private List<Point> bufPointList;

        @Override
        protected String doInBackground(Context... params) {
            int i = 0;
            while (true) {
                try {
                    Thread.sleep(500);
                    bufPointList = getOldPointsList();

                    publishProgress(i);
                    i++;
                } catch (Exception e) {

                }
            }
            // return "COMPLETE!";
        }

        // -- gets called just before thread begins
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            for (Point aBufPointList : bufPointList) {
                x++;
                mXSeries.add(x, aBufPointList.x);
                mYSeries.add(x, aBufPointList.y);
                mZSeries.add(x, aBufPointList.z);
                //mForceSeries.add(x, aBufPointList.force);
            }

            if (mChartView != null) {
                mChartView.repaint();
            }

            Bitmap bitmap = mChartView.toBitmap();
            try {
                File file = new File(Environment.getExternalStorageDirectory(),
                        "test" + index++ + ".png");
                FileOutputStream output = new FileOutputStream(file);
                bitmap.compress(CompressFormat.PNG, 100, output);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        // -- called if the cancel button is pressed
        @Override
        protected void onCancelled() {
            super.onCancelled();

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }
}
