package com.sielc.designtest;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sielc on 6/23/2015.
 */
public class SequenceTabFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.sequence_table_fragment, container, false);

        ListView listView = (ListView)view.findViewById(R.id.listView);
        List<SequenceTableItemModel> list = new ArrayList<>();
        list.add(new SequenceTableItemModel());

        listView.setAdapter(new SequenceTableAdapter(getActivity().getApplicationContext(), list));

        return view;
    }
}
