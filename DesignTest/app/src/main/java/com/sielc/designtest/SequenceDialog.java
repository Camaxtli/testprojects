package com.sielc.designtest;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.View;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabWidget;

import java.util.ArrayList;
import java.util.List;

public class SequenceDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.sequence_dialog, null);
        builder.setView(view);

        ListView listView = (ListView)view.findViewById(R.id.listView);
        List<SequenceTableItemModel> list = new ArrayList<>();
        list.add(new SequenceTableItemModel());

        listView.setAdapter(new SequenceTableAdapter(getActivity().getApplicationContext(), list));

        return builder.create();
    }
}
