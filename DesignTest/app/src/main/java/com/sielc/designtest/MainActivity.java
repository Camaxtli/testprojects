package com.sielc.designtest;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.Toast;

import com.sielc.designtest.util.ContactInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends Activity {

    private Map views = new HashMap();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        RecyclerView recList = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);

        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        recList.setLayoutManager(llm);

        ContactAdapter ca = new ContactAdapter(createList(5));
        recList.setAdapter(ca);

        Button sequenceBtn = (Button)findViewById(R.id.sequenceBtn);
        sequenceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //FragmentManager fm = getFragmentManager();
                //SequenceDialog seqDlg = new SequenceDialog();
                //seqDlg.show(fm, "sequence_dialog");

                SequenceTabFragment fragment = new SequenceTabFragment();
                FragmentTransaction ft = getFragmentManager()
                        .beginTransaction();
                ft.replace(R.id.main_frame, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }
        });
    }

    private List<ContactInfo> createList(int size) {

        List<ContactInfo> result = new ArrayList<ContactInfo>();
        for (int i=1; i <= size; i++) {
            ContactInfo ci = new ContactInfo();
            ci.name = ContactInfo.NAME_PREFIX + i;

            result.add(ci);

        }

        return result;
    }

    public void selfDestruct(View view) {
        if (views.containsKey(view))
            if (views.get(view) == true)
            {
                ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
                ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 1f);
                ObjectAnimator animE1 = ObjectAnimator.ofFloat(view, "elevation", 4f);
                AnimatorSet animSetXY1 = new AnimatorSet();
                animSetXY1.playTogether(animX1, animY1, animE1);
                animSetXY1.setDuration(500);
                animSetXY1.start();
                views.put(view, false);
                return;
            }
        for(Object key : views.keySet())
        {
            if (views.get(key) == true)
            {
                ObjectAnimator animX2 = ObjectAnimator.ofFloat((View)key, "scaleX", 1f);
                ObjectAnimator animY2 = ObjectAnimator.ofFloat((View)key, "scaleY", 1f);
                ObjectAnimator animE2 = ObjectAnimator.ofFloat((View)key, "elevation", 4f);
                AnimatorSet animSetXY2 = new AnimatorSet();
                animSetXY2.playTogether(animX2, animY2, animE2);
                animSetXY2.setDuration(500);
                animSetXY2.start();
                views.put((View)key, false);
                break;
            }
        }

        ObjectAnimator animX = ObjectAnimator.ofFloat(view, "scaleX", 1.3f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(view, "scaleY", 1.3f);
        ObjectAnimator animE = ObjectAnimator.ofFloat(view, "elevation", 25f);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animE);
        animSetXY.setDuration(500);
        animSetXY.start();
        views.put(view, true);
    }
}
