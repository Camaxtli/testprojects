package com.sielc.designtest;

public class SequenceTableItemModel {
    public int sequenceNumber;
    public int vialNumber;
    public String sampleName;
    public String methodName;
    public int injectionVialNumber;
    public int injectionVolume;
    public int sampleType;
    public int amountValue;
    public int amountISTDValue;
    public int dilution;
}
