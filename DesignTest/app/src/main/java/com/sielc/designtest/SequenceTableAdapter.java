package com.sielc.designtest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sielc.designtest.R;
import com.sielc.designtest.SequenceTableItemModel;

import java.util.List;

public class SequenceTableAdapter extends ArrayAdapter<SequenceTableItemModel> {
    private final Context context;
    private final List<SequenceTableItemModel> values;

    public SequenceTableAdapter(Context context, List<SequenceTableItemModel> values) {
        super(context, R.layout.sequence_table_item, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.sequence_table_item, parent, false);
        EditText sequenceNumber = (EditText) rowView.findViewById(R.id.sequence_number);
        sequenceNumber.setText(Integer.toString(1));

        return rowView;
    }
} 
