package com.sielc.designtest;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class TrayView extends View {

    private Paint mViewPaint;
    int[][] trayMap;
    float initialX, initialY;

    Map<Point, RectF> vialsMap;

    int selectedVial;
    String vialInitColor;
    String vialSelectingColor;
    String vialSelectedColor;

    VialSelectionListener listener;

    public TrayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mViewPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        trayMap = new int[][] {
                { 0,30, 0,29, 0,28, 0,27, 0},
                {17, 0,26, 0,25, 0,24, 0,40},
                { 0,14, 0,23, 0,22, 0,36, 0},
                {18, 0,12, 0,21, 0,33, 0,39},
                { 0,15, 0,11, 0,31, 0,35, 0},
                {19, 0,13, 0, 1, 0,32, 0,38},
                { 0,16, 0, 2, 0, 3, 0,34, 0},
                {20, 0, 4, 0, 5, 0, 6, 0,37},
                { 0, 7, 0, 8, 0, 9, 0,10, 0}
        };

        vialsMap = new HashMap<>();
        selectedVial = 0;
        vialInitColor = "#FFFFFF";
        vialSelectingColor = "#BDBDBD";
        vialSelectedColor = "#CFD8DC";
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        float marginAllInDp = 5;  // View's margin in dp
        int marginAll = (int)convertDpToPixel(marginAllInDp, getContext());
        int width = getWidth();
        int height = getHeight();
        int centerX = getWidth() / 2;
        int centerY = getHeight() / 2;

        // Background
        mViewPaint.setStyle(Paint.Style.FILL);
        mViewPaint.setColor(Color.WHITE);
        canvas.drawPaint(mViewPaint);

        canvas.rotate(45, centerX, centerY);

        RectF viewRect = new RectF(marginAll, marginAll, width - marginAll, height - marginAll);
        Path path1 = new Path();
        mViewPaint.setColor(Color.parseColor("#A5D6A7"));
        path1.moveTo(centerX, centerY);
        path1.arcTo(viewRect, 0, 90);
        path1.lineTo(centerX, centerY);
        canvas.drawPath(path1, mViewPaint);

        Path path2 = new Path();
        mViewPaint.setColor(Color.parseColor("#FFF59D"));
        path2.moveTo(centerX, centerY);
        path2.arcTo(viewRect, 90, 90);
        path2.lineTo(centerX, centerY);
        canvas.drawPath(path2, mViewPaint);

        Path path3 = new Path();
        mViewPaint.setColor(Color.parseColor("#FFAB91"));
        path3.moveTo(centerX, centerY);
        path3.arcTo(viewRect, 180, 90);
        path3.lineTo(centerX, centerY);
        canvas.drawPath(path3, mViewPaint);

        Path path4 = new Path();
        mViewPaint.setColor(Color.parseColor("#64B5F6"));
        path4.moveTo(centerX, centerY);
        path4.arcTo(viewRect, 270, 90);
        path4.lineTo(centerX, centerY);
        canvas.drawPath(path4, mViewPaint);

        canvas.rotate(-45, centerX, centerY);

        // Crossing lines
        mViewPaint.setColor(Color.WHITE);
        mViewPaint.setStrokeWidth(4);
        canvas.drawLine(0, 0, width, height, mViewPaint);
        canvas.drawLine(0, height, width, 0, mViewPaint);

        // Round circle
        mViewPaint.setStyle(Paint.Style.STROKE);
        mViewPaint.setColor(Color.BLACK);
        mViewPaint.setStrokeWidth(2);
        canvas.drawCircle(centerX, centerY, height / 2 - marginAll, mViewPaint);

        // Vials
        mViewPaint.setStyle(Paint.Style.FILL);
        mViewPaint.setColor(Color.WHITE);

        mViewPaint.setStrokeWidth(2);

        int margin = 20;        // Margin inside the circle
        float step = (width - 2*marginAll - 2*margin) / 11;
        float halfstep = step/2;
        int radius = (int)convertDpToPixel(28, getContext());
        int rowsCount = 11;
        int columnsCount = 11;

        for(int i=1; i<rowsCount - 1; i++) {
            for (int j=1; j<columnsCount - 1; j++) {
                float y = margin + marginAll + halfstep + i*step;
                float x = margin + marginAll + halfstep + j*step;

                if (((i & 1) == 0 && (j & 1) != 0) || ((j & 1) == 0 && (i & 1) != 0)) {

                    if (selectedVial == trayMap[i-1][j-1]) {
                        mViewPaint.setColor(Color.parseColor(vialSelectedColor));
                        listener.onVialSelected(selectedVial);
                    }
                    else
                        mViewPaint.setColor(Color.parseColor(vialInitColor));

                    canvas.drawCircle(x, y, radius, mViewPaint);
                    mViewPaint.setColor(Color.DKGRAY);

                    // Fill vialMap
                    vialsMap.put(new Point(i-1, j-1), new RectF(x-halfstep, y-halfstep, x+halfstep, y+halfstep));

                    int value = trayMap[i-1][j-1]%10;
                    if (value == 0)
                        value = 10;
                    String label = new String(Integer.toString(value));

                    drawLabels(x, y, label, canvas);
                }
            }
        }

        // Tray quarter labels
        Typeface font = Typeface.create("Helvetica", Typeface.BOLD);
        mViewPaint.setTypeface(font);
        mViewPaint.setTextSize(convertDpToPixel(32, getContext()));

        String []labels = new String[] {"A", "B", "C", "D"};
        int shift = (int)convertDpToPixel(25, getContext());
        Point []points = new Point[] {
                new Point(marginAll + shift, centerY),
                new Point(centerX, marginAll + shift),
                new Point(width - marginAll - shift, centerY),
                new Point(centerX, height - marginAll - shift)};

        for(int i = 0; i< labels.length; i++)
        {
            Rect bounds = new Rect();
            mViewPaint.getTextBounds(labels[i], 0, labels[i].length(), bounds);
            canvas.drawText(labels[i], points[i].x - bounds.centerX(), points[i].y - bounds.centerY(), mViewPaint);
        }

        super.onDraw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        String TAG = "TEST_DEBUG";
        int action = event.getActionMasked();

        switch (action) {

            case MotionEvent.ACTION_DOWN:
                initialX = event.getX();
                initialY = event.getY();
                break;

            case MotionEvent.ACTION_UP:
                float finalX = event.getX();
                float finalY = event.getY();;

                boolean tapPerformed = true;
                if (initialX < finalX) {
                    Log.d(TAG, "Left to Right swipe performed");
                    tapPerformed = false;
                }
                if (initialX > finalX) {
                    Log.d(TAG, "Right to Left swipe performed");
                    tapPerformed = false;
                }
                if (initialY < finalY) {
                    Log.d(TAG, "Up to Down swipe performed");
                    tapPerformed = false;
                }
                if (initialY > finalY) {
                    Log.d(TAG, "Down to Up swipe performed");
                    tapPerformed = false;
                }

                if (tapPerformed)
                {
                    findVialNumberByCoords(finalX, finalY);
                    invalidate();
                }

                break;
        }
        return true;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

    }

    public void drawLabels(float x, float y, String text, Canvas canvas)
    {
        Typeface font = Typeface.create("Helvetica", Typeface.BOLD);
        mViewPaint.setTypeface(font);
        mViewPaint.setTextSize(convertDpToPixel(18, getContext()));

        Rect bounds = new Rect();
        mViewPaint.getTextBounds(text, 0, text.length(), bounds);

        canvas.drawText(text, x - bounds.centerX(), y - bounds.centerY(), mViewPaint);
    }

    public void findVialNumberByCoords(float x, float y)
    {
        Iterator it = vialsMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            RectF rectF = (RectF)pair.getValue();
            if(rectF.contains(x, y)) {
                Point point = (Point)pair.getKey();
                int newSelectedVial = trayMap[point.x][point.y];
                if (newSelectedVial == selectedVial)
                    selectedVial = 0;
                else
                    selectedVial = newSelectedVial;
            }
        }
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }



    public interface VialSelectionListener {
        public void onVialSelected(int selectedVial);
    }

}
